<?php

declare(strict_types = 1);

namespace App\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class FlourChart extends BaseChart
{
    private $flourName = "Tepung 1 Dus";
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     * @param Request $request
     * @return Chartisan
     */
    public function handler(Request $request): Chartisan
    {
        /** @var 2017 */
        $jan2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-01' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $feb2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-02' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mar2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-03' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $apr2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-04' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mei2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-05' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jun2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-06' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jul2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-07' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $agus2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-08' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $sep2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-09' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $okt2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-10' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $nov2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-11' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $des2017 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2017-12' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();

        /** @var 2018 */
        $jan2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-01' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $feb2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-02' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mar2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-03' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $apr2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-04' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mei2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-05' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jun2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-06' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jul2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-07' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $agus2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-08' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $sep2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-09' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $okt2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-10' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $nov2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-11' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $des2018 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2018-12' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();

        /** @var 2019 */
        $jan2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-01' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $feb2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-02' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mar2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-03' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $apr2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-04' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $mei2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-05' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jun2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-06' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $jul2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-07' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $agus2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-08' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $sep2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-09' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $okt2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-10' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $nov2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-11' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();
        $des2019 = \DB::table('ingredients')
            ->join('purchases', 'ingredients.id', '=', 'purchases.ingredient_id')
            ->select('ingredients.name')
            ->where('purchases.date', 'LIKE', '%'. '2019-12' .'%')
            ->where('ingredients.name', $this->flourName)
            ->get()
            ->count();


        return Chartisan::build()
            ->labels([
                'January',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
            ])
            ->dataset('2017', [$jan2017, $feb2017, $mar2017, $apr2017, $mei2017, $jun2017, $jul2017, $agus2017, $sep2017, $okt2017, $nov2017, $des2017])
            ->dataset('2018', [$jan2018, $feb2018, $mar2018, $apr2018, $mei2018, $jun2018, $jul2018, $agus2018, $sep2018, $okt2018, $nov2018, $des2018])
            ->dataset('2019', [$jan2019, $feb2019, $mar2019, $apr2019, $mei2019, $jun2019, $jul2019, $agus2019, $sep2019, $okt2019, $nov2019, $des2019]);
    }
}
