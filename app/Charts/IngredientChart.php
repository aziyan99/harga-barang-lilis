<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Ingredient;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class IngredientChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     * @param Request $request
     * @return Chartisan
     */
    public function handler(Request $request): Chartisan
    {
        $rawIngredients = Ingredient::all();
        $ingredients = [];
        foreach ($rawIngredients as $data){
            $save = $data->name;
            array_push($ingredients, $save);
        }
        $purchases = [];
        foreach ($rawIngredients as $data){
            $save = $data->purchases()->count();
            array_push($purchases, $save);
        }

        return Chartisan::build()
            ->labels($ingredients)
            ->dataset('Pembelian', $purchases);
    }
}
