<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Purchase;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class PurchaseChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     * @param Request $request
     * @return Chartisan
     */
    public function handler(Request $request): Chartisan
    {
        /** @var  2017 */
        $jan2017 = Purchase::where('date', 'LIKE', '%'. '2017-01' .'%')->get()->count();
        $feb2017 = Purchase::where('date', 'LIKE', '%'. '2017-02' .'%')->get()->count();
        $mar2017 = Purchase::where('date', 'LIKE', '%'. '2017-03' .'%')->get()->count();
        $apr2017 = Purchase::where('date', 'LIKE', '%'. '2017-04' .'%')->get()->count();
        $mei2017 = Purchase::where('date', 'LIKE', '%'. '2017-05' .'%')->get()->count();
        $jun2017 = Purchase::where('date', 'LIKE', '%'. '2017-06' .'%')->get()->count();
        $jul2017 = Purchase::where('date', 'LIKE', '%'. '2017-07' .'%')->get()->count();
        $agus2017 = Purchase::where('date', 'LIKE', '%'. '2017-08' .'%')->get()->count();
        $sep2017 = Purchase::where('date', 'LIKE', '%'. '2017-09' .'%')->get()->count();
        $okto2017 = Purchase::where('date', 'LIKE', '%'. '2017-10' .'%')->get()->count();
        $niv2017 = Purchase::where('date', 'LIKE', '%'. '2017-11' .'%')->get()->count();
        $des2017 = Purchase::where('date', 'LIKE', '%'. '2017-12' .'%')->get()->count();
        /** @var  2018 */
        $jan2018 = Purchase::where('date', 'LIKE', '%'. '2018-01' .'%')->get()->count();
        $feb2018 = Purchase::where('date', 'LIKE', '%'. '2018-02' .'%')->get()->count();
        $mar2018 = Purchase::where('date', 'LIKE', '%'. '2018-03' .'%')->get()->count();
        $apr2018 = Purchase::where('date', 'LIKE', '%'. '2018-04' .'%')->get()->count();
        $mei2018 = Purchase::where('date', 'LIKE', '%'. '2018-05' .'%')->get()->count();
        $jun2018 = Purchase::where('date', 'LIKE', '%'. '2018-06' .'%')->get()->count();
        $jul2018 = Purchase::where('date', 'LIKE', '%'. '2018-07' .'%')->get()->count();
        $agus2018 = Purchase::where('date', 'LIKE', '%'. '2018-08' .'%')->get()->count();
        $sep2018 = Purchase::where('date', 'LIKE', '%'. '2018-09' .'%')->get()->count();
        $okto2018 = Purchase::where('date', 'LIKE', '%'. '2018-10' .'%')->get()->count();
        $niv2018 = Purchase::where('date', 'LIKE', '%'. '2018-11' .'%')->get()->count();
        $des2018 = Purchase::where('date', 'LIKE', '%'. '2018-12' .'%')->get()->count();
        /** @var  2019 */
        $jan2019 = Purchase::where('date', 'LIKE', '%'. '2019-01' .'%')->get()->count();
        $feb2019 = Purchase::where('date', 'LIKE', '%'. '2019-02' .'%')->get()->count();
        $mar2019 = Purchase::where('date', 'LIKE', '%'. '2019-03' .'%')->get()->count();
        $apr2019 = Purchase::where('date', 'LIKE', '%'. '2019-04' .'%')->get()->count();
        $mei2019 = Purchase::where('date', 'LIKE', '%'. '2019-05' .'%')->get()->count();
        $jun2019 = Purchase::where('date', 'LIKE', '%'. '2019-06' .'%')->get()->count();
        $jul2019 = Purchase::where('date', 'LIKE', '%'. '2019-07' .'%')->get()->count();
        $agus2019 = Purchase::where('date', 'LIKE', '%'. '2019-08' .'%')->get()->count();
        $sep2019 = Purchase::where('date', 'LIKE', '%'. '2019-09' .'%')->get()->count();
        $okto2019 = Purchase::where('date', 'LIKE', '%'. '2019-10' .'%')->get()->count();
        $niv2019 = Purchase::where('date', 'LIKE', '%'. '2019-11' .'%')->get()->count();
        $des2019 = Purchase::where('date', 'LIKE', '%'. '2019-12' .'%')->get()->count();
        return Chartisan::build()
            ->labels([
                'January',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
            ])
            ->dataset('2017', [$jan2017, $feb2017, $mar2017, $apr2017, $mei2017, $jun2017, $jul2017, $agus2017, $sep2017, $okto2017, $niv2017, $des2017])
            ->dataset('2018', [$jan2018, $feb2018, $mar2018, $apr2018, $mei2018, $jun2018, $jul2018, $agus2018, $sep2018, $okto2018, $niv2018, $des2018])
            ->dataset('2019', [$jan2019, $feb2019, $mar2019, $apr2019, $mei2019, $jun2019, $jul2019, $agus2019, $sep2019, $okto2019, $niv2019, $des2019]);
    }
}
