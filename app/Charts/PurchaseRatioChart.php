<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\Purchase;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class PurchaseRatioChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $data2017 = Purchase::where('date', 'LIKE', '%'. '2017-' .'%')->get()->count();
        $data2018 = Purchase::where('date', 'LIKE', '%'. '2018-' .'%')->get()->count();
        $data2019 = Purchase::where('date', 'LIKE', '%'. '2019-' .'%')->get()->count();

        return Chartisan::build()
            ->labels(['2017', '2018', '2019'])
            ->dataset('Pembelian', [$data2017, $data2018, $data2019]);
    }
}
