<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultPageController extends Controller
{
    public function index()
    {
        return view('defaults.home');
    }
}
