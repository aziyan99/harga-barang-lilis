<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use App\Models\Purchase;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $results = NULL;
    protected $type = NULL;

    public function index()
    {
        return view('search.index');
    }

    public function search(Request $request)
    {
        if($request->type == "name_s"){
            $this->results = Ingredient::with('purchases')->where('name', 'LIKE', "%$request->name%")->get();
            $this->type = "name_s";
        }else{
            $this->results = Purchase::with('ingredient')->where('date', $request->date)->get();
            $this->type = "date_s";
        }
        return view('search.results', [
            'results' => $this->results,
            'type' => $this->type
        ]);
    }

    public function searchByMonthAndYear(Request $request)
    {
        $date = $request->year . "-" . $request->month;
        $purchases = Purchase::where('date', 'LIKE', '%'. $date .'%')->with('ingredient')->get();
        $dateRes = [
            'month' => $request->month,
            'year' => $request->year
        ];
        return view('search.custom', compact('purchases', 'dateRes'));
    }
}
