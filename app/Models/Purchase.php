<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = [
        'ingredient_id', 'qty', 'price', 'date', 'desc'
    ];

    public function ingredient()
    {
        return $this->belongsTo('App\Models\Ingredient');
    }
}
