@extends('layouts.main')

@section('title', 'Home')

@section('content')
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-12">
                <!-- Chart's container -->
                <div id="allPurchases" style="height: 500px;"></div>
                <br>
                <br>
                <div id="flourPurchases" style="height: 500px;"></div>
                <br>
                <br>
                <div id="purchaseRatio" style="height: 500px;"></div>
                <br>
                <br>
                <div id="ingredient" style="height: 500px;"></div>
                <br>
                <br>
            </div>
            <!-- Charting library -->
            <script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
            <!-- Chartisan -->
            <script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script>
            <!-- Your application script -->
            <script>
                const chart = new Chartisan({
                    el: '#allPurchases',
                    url: "@chart('purchase_chart')",
                    loader: {
                        color: '#ff00ff',
                        size: [30, 30],
                        type: 'bar',
                        textColor: '#ffff00',
                        text: 'Loading some chart data...',
                    },
                    hooks: new ChartisanHooks()
                        .colors()
                        .legend({ position: 'bottom' })
                        .title('Pembelian bahan 2017 - 2019')
                        .tooltip()
                        .datasets(['bar', 'bar', 'bar']),
                });
                const flourChart = new Chartisan({
                    el: '#flourPurchases',
                    url: "@chart('flour_chart')",
                    loader: {
                        color: '#ff00ff',
                        size: [30, 30],
                        type: 'bar',
                        textColor: '#ffff00',
                        text: 'Loading some chart data...',
                    },
                    hooks: new ChartisanHooks()
                        .colors()
                        .legend({ position: 'bottom' })
                        .tooltip()
                        .title('Pembelian tepung 2017 - 2019')
                        .datasets(['bar', 'bar', 'bar']),
                });
                const purchaseRatio = new Chartisan({
                    el: '#purchaseRatio',
                    url: "@chart('purchase_ratio_chart')",
                    loader: {
                        color: '#ff00ff',
                        size: [30, 30],
                        type: 'bar',
                        textColor: '#ffff00',
                        text: 'Loading some chart data...',
                    },
                    hooks: new ChartisanHooks()
                        .colors()
                        .legend({ position: 'bottom' })
                        .title('Perbandingan pembelian bahan 2017 - 2019')
                        .datasets(['line'])
                        .tooltip()
                });
                const ingredients = new Chartisan({
                    el: '#ingredient',
                    url: "@chart('ingredient_chart')",
                    loader: {
                        color: '#ff00ff',
                        size: [30, 30],
                        type: 'bar',
                        textColor: '#ffff00',
                        text: 'Loading some chart data...',
                    },
                    hooks: new ChartisanHooks()
                        .colors()
                        .legend({ position: 'bottom' })
                        .title('Perbandingan bahan-bahan 2017 - 2019')
                        .datasets(['bar'])
                        .tooltip()
                });
            </script>
        </div>
    </div>
@endsection
