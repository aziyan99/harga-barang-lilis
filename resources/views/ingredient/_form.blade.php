<div class="form-group">
    <label>Nama Barang</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" autofocus autocomplete="off" value="{{ old('name', $ingredient->name) }}">
    @error('name')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label>Keterangan</label>
    <textarea name="desc" cols="30" rows="4" class="form-control @error('desc') is-invalid @enderror">{{ old('desc', $ingredient->desc) }}</textarea>
    @error('desc')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
