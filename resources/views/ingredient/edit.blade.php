@extends('layouts.main')

@section('title', 'Bahan - Bahan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h3 class="card-title">Edit Barang</h3>
                    <form action="{{ route('ingredients.update', $ingredient->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        @include('ingredient._form')
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm" type="submit"><i class="bi bi-check2 mr-2"></i>Ubah</button>
                            <a href="{{ route('ingredients.index') }}" class="btn btn-sm btn-secondary"><i class="bi bi-arrow-left mr-2"></i>Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
