@extends('layouts.main')

@section('title', 'Bahan - Bahan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-header">
                    <h3 class="card-title">Data Barang</h3>
                    <small>Semua data barang yang tersimpan di sistem</small>
                </div>
                <div class="card-body">
                    <a href="{{ route('ingredients.create') }}" class="btn btn-sm btn-success mt-3"><i class="bi bi-bag-plus mr-2"></i>Tambah</a>
                    <table class="mt-3 table-sm table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Barang</th>
                                <th>Keterangan</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($ingredients as $ingredient)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $ingredient->name }}</td>
                                <td>{{ $ingredient->desc }}</td>
                                <td>
                                    <a href="{{ route('ingredients.show', $ingredient->id) }}"
                                        class="btn btn-info btn-sm"><i class="bi bi-bag-check mr-2"></i>Detail</a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center text-muted"><i>Barang kosong</i></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
