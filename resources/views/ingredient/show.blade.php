@extends('layouts.main')

@section('title', 'Bahan - Bahan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h3 class="card-title">Detail Barang</h3>

                    <table class="table table-hover table-bordered mt-3 mb-3">
                        <tr>
                            <th>Nama Barang</th>
                            <td>{{ $ingredient->name }}</td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <td>{{ $ingredient->desc }}</td>
                        </tr>
                    </table>
                    <div class="form-group">
                        <a href="{{ route('ingredients.index') }}" class="btn btn-sm btn-secondary"><i class="bi bi-arrow-left mr-2"></i>Kembali</a>
                        <a href="{{ route('ingredients.edit', $ingredient->id) }}" class="btn btn-sm btn-warning"><i class="bi bi-pencil mr-2"></i>Edit</a>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal">
                            <i class="bi bi-trash mr-2"></i>Hapus
                        </button>
                    </div>

                    <h3 class="card-title mt-3">Riwayat Pembelian</h3>
                    <table class="table table-sm table-bordered table-hover mt-2 mb-3">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tanggal Pembelian</th>
                                <th>QTY</th>
                                <th>Harga</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($ingredient->purchases as $purchase)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $purchase->date }}</td>
                                    <td>{{ $purchase->qty }}</td>
                                    <td>Rp. {{ number_format($purchase->price,2,",",".") }}</td>
                                    <td>{{ $purchase->desc }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center text-muted"><i>Tidak ada Pembelian atas barang ini</i></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Hapus barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{ route('ingredients.destroy',  $ingredient->id) }}" method="POST">
            <div class="modal-body">
                <div class="alert alert-danger">Hapus barang ini?</div>
                    @csrf
                    @method('DELETE')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="bi bi-x-circle mr-2"></i>Batal</button>
                <button class="btn btn-danger btn-sm"><i class="bi bi-trash mr-2"></i>Hapus</button>
        </form>
        </div>
    </div>
</div>
@endsection
