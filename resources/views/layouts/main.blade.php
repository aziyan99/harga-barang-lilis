<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <title>@yield('title') - ANALISA KILANG ROTI AMIR BURGER</title>
	@stack('links')
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light px-5 py-2 border border-top-0 border-right-0 border-left-0">
        <div class="container-fluid">
        <a class="navbar-brand" href="/"><i class="bi bi-graph-up"></i></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <form class="form-inline my-2">
                <input class="form-control mr-sm-2" placeholder="Command [ / ]" aria-label="Search">
            </form>
            <div class="navbar-nav ml-auto">
                <a class="nav-link" href="/"><i class="bi bi-house-door mr-1"></i>Rumah</a>
                <a class="nav-link" href="{{ route('ingredients.index') }}"><i class="bi bi-bag mr-1"></i>Barang</a>
                <a class="nav-link" href="{{ route('purchases.index') }}"><i class="bi bi-cart mr-1"></i>Pembelian</a>
                <a class="nav-link" href="{{ route('search.index') }}"><i class="bi bi-search"></i></a>
            </div>
        </div>
    </div>
    </nav>

    @yield('content')

    <footer class="px-3 py-5">
        <div class="container-fluid mt-2">
            <div class="row">
                <div class="col-12 text-center">
                    <small class="text-muted"><i>&copy; 2020 Lilis Riskha Agustini</i></small>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
	@stack('scripts')
</body>

</html>
