<div class="form-group">
    <label>Nama Barang</label>
    <select name="ingredient_id" class="form-control @error('ingredient_id') is-invalid @enderror">
        @foreach ($ingredients as $ingredient)
            <option value="{{ $ingredient->id }}" {{ (old('ingredient_id', $purchase->ingredient_id) == $ingredient->id) ? 'selected' : '' }}>
                {{ $ingredient->name }}
            </option>
        @endforeach
    </select>
    @error('ingredient_id')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label>QTY</label>
    <input type="number" name="qty" class="form-control @error('qty') is-invalid @enderror" value="{{ old('qty', $purchase->qty) }}">
    @error('qty')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label>Harga</label>
    <input type="number" name="price" class="form-control @error('price') is-invalid @enderror" value="{{ old('price', $purchase->price) }}">
    @error('price')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label>Tanggal</label>
    <input type="date" name="date" class="form-control @error('date') is-invalid @enderror" value="{{ old('date', $purchase->date) }}">
    @error('date')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label>Keterangan</label>
    <textarea name="desc" cols="30" rows="4" class="form-control @error('desc') is-invalid @enderror">{{ old('desc', $purchase->desc) }}</textarea>
    @error('desc')
        <small class="invalid-feedback" role="alert">{{ $message }}</small>
    @enderror
</div>
