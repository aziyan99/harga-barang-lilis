@extends('layouts.main')

@section('title', 'Pembelian')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-header">
                    <h3 class="card-title">Data Pembelian</h3>
                    <small>Semua data pembelian yang tersimpan di sistem</small>
                </div>
                <div class="card-body">
                    <a href="{{ route('purchases.create') }}" class="mb-3 btn btn-sm btn-success mt-3"><i class="bi bi-cart-plus mr-2"></i>Tambah</a>
                    <div class="table-responsive">
                        <table id="purchasesData" class="mt-3 table-sm table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Barang</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Tanggal beli</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($purchases as $purchase)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $purchase->ingredient->name }}</td>
                                    <td>{{ $purchase->qty }}</td>
                                    <td>Rp. {{ number_format($purchase->price,2,",",".") }}</td>
                                    <td>{{ $purchase->date }}</td>
                                    <td>
                                        <a href="{{ route('purchases.show', $purchase->id) }}"
                                            class="btn btn-sm btn-info"><i class="bi bi-cart-check mr-2"></i>Detail</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6" class="text-center text-muted"><i>Pembelian kosong</i></td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('links')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.css"/>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.js"></script>
<script>
$(document).ready( function () {
    $('#purchasesData').DataTable();
});
</script>
@endpush

