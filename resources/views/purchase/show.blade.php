@extends('layouts.main')

@section('title', 'Pembelian')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h3 class="card-title">Detail Pembelian</h3>

                    <table class="table table-hover table-bordered mt-3 mb-3">
                        <tr>
                            <th>Nama Barang</th>
                            <td>{{ $purchase->ingredient->name }}</td>
                        </tr>
                        <tr>
                            <th>QTY</th>
                            <td>{{ $purchase->qty }}</td>
                        </tr>
                        <tr>
                            <th>Harga</th>
                            <td>Rp. {{ number_format($purchase->price,2,",",".") }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <td>{{ $purchase->date }}</td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <td>{{ $purchase->desc }}</td>
                        </tr>
                    </table>
                    <div class="form-group">
                        <a href="{{ route('purchases.index') }}" class="btn btn-sm btn-secondary"><i class="bi bi-arrow-left mr-2"></i>Kembali</a>
                        <a href="{{ route('purchases.edit', $purchase->id) }}" class="btn btn-sm btn-warning"><i class="bi bi-pencil mr-2"></i>Edit</a>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal">
                            <i class="bi bi-trash mr-2"></i> Hapus
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Hapus barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{ route('purchases.destroy',  $purchase->id) }}" method="POST">
            <div class="modal-body">
                <div class="alert alert-danger">Hapus pembelian ini?</div>
                    @csrf
                    @method('DELETE')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="bi bi-x-circle mr-2"></i>Batal</button>
                <button class="btn btn-sm btn-danger"><i class="bi bi-trash mr-2"></i>Hapus</button>
        </form>
        </div>
    </div>
</div>
@endsection
