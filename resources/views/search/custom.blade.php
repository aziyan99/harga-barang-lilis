@extends('layouts.main')

@section('title', 'Pembelian')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Hasil Pencarian</h3>
                        <small>Hasil dari pencarian anda untuk bulan <b>{{ date("F", strtotime($dateRes['month'])) }}</b>
                            pada tahun <b>{{ $dateRes['year'] }}</b></small>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-bordered table-sm">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama barang</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Tanggal pembelian</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($purchases as $purchase)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $purchase->ingredient->name }}</td>
                                    <td>{{ $purchase->qty }}</td>
                                    <td>Rp. {{ number_format($purchase->price,2,",",".") }}</td>
                                    <td>{{ $purchase->date }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center text-muted"><i>Tidak ada data</i></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <a href="{{ route('search.index') }}" class="btn btn-sm btn-secondary"><i
                                class="bi bi-arrow-left mr-2"></i>Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
