@extends('layouts.main')

@section('title', 'Pembelian')

@section('content')
<div class="container">
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Cari perbulan dan tahun</h3>
                    <small>Cari data berdasarkan bulan dan tahun</small>
                </div>
                <div class="card-body">
                    <form action="{{ route('search.custom') }}">
                        @csrf
                        <div class="form-group">
                            <label>Tahun</label>
                            <select name="year" class="form-control">
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bulan</label>
                            <select name="month" class="form-control">
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="bi bi-search mr-2"></i>Cari</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
