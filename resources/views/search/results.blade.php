@extends('layouts.main')

@section('title', 'Pembelian')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h3 class="card-title">Hasil Pencarian</h3>
                    @if ($type == "name_s")
                    @forelse ($results as $result)
                    <ul>
                        <li>Nama Barang: <b>{{ $result->name }}</b>
                            <ul>
                                @foreach ($result->purchases as $purchase)
                                <ul>
                                    <li>QTY: {{ $purchase->qty }}</li>
                                    <li>Harga: {{ $purchase->price }}</li>
                                    <li>Tanggal: {{ $purchase->date }}</li>
                                    <li>Keterangan: {{ $purchase->desc }}</li>
                                </ul>
                                ---
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                    @empty
                    <span class="text-muted"><i>Tidak ada hasil ...</i></span>
                    <br>
                    @endforelse

                    @else
                    @forelse ($results as $result)
                    <ul>
                        <li>Nama Barang: <b>{{ $result->ingredient->name }}</b></li>
                        <li>QTY: {{ $result->qty }}</li>
                        <li>Harga: {{ $result->price }}</li>
                        <li>Tanggal: {{ $result->date }}</li>
                        <li>Keterangan: {{ $result->desc }}</li>
                    </ul>
                    @empty
                    <span class="text-muted"><i>Tidak ada hasil ...</i></span>
                    <br>
                    @endforelse
                    @endif
                    <a href="{{ route('search.index') }}" class="btn btn-sm btn-secondary"><i
                            class="bi bi-arrow-left mr-2"></i>Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
