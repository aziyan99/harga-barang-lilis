<?php

use App\Http\Controllers\ChartController;
use App\Http\Controllers\DefaultPageController;
use App\Http\Controllers\IngredientController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DefaultPageController::class, 'index'])->name('home.index');

Route::resource('ingredients', IngredientController::class);
Route::resource('purchases', PurchaseController::class);

Route::get('/search', [SearchController::class, 'index'])->name('search.index');
Route::post('/search', [SearchController::class, 'search'])->name('search.search');
Route::get('/search-by-year-and-month', [SearchController::class, 'searchByMonthAndYear'])->name('search.custom');
